# Weather Application
![Design homepage of the Weather Application](./images/homepage.png)

## Description
This is a responsive Weather Application using HTML/CSS and Javascript. With this application you can see the weather of the country that you put on the input. I use openweathermap API with a key.

## Contains
Call the api with fetch

Show the data via search

Change the background and the background-box dynamically


## Technologies Used
Html

Scss

Javascript

OpenWeatherMap API


## Installation
git clone https://gitlab.com/Yutsu/weatherjsapi


## Start Server
Live server from Vscode Extension and click "Go live"

## Ressources

**Bequia Stories**

https://islandlifestories.com/canadian-or-vicentian/

**Maria Orlova**

https://www.pexels.com/photo/peaceful-village-on-hilltop-in-picturesque-highlands-4946445/

**Starry sky**

https://jooinn.com/silhouette-of-trees-during-night-time-3.html

**Dziana Hasanbekava**

https://www.pexels.com/photo/anonymous-hiker-recreating-on-rocky-cliff-and-enjoying-mountain-view-at-sundown-5590718/
