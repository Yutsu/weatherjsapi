const apikey = "3265874a2c77ae4a04bb96236a642d2f";
const URL = "https://api.openweathermap.org/data/2.5/weather?q="

//`https://api.openweathermap.org/data/2.5/weather?q={paris}&appid={apikey}`;
export const apiFetch = async(city) => {
    try {
        const result = await fetch(`${URL}${city}&appid=${apikey}`, { origin: "cors" });
        if(result.ok === false) {
            return "Not found";
        }
        const data = await result.json();
        return data;
    } catch (error) {
        console.error(error);
    }
}
