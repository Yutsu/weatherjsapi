import { getClass, getId } from "./getElement.js";
import { apiFetch } from "./api.js";

const container = getClass('.container');
const containerBox = getClass('.container-box');

const form = getId('form');
const icon = getClass('.icon');
const nameCity = getClass('.nameCity');
const deg = getClass('.deg');
const humid = getClass('.humid');
const temp = getClass('.temp');
const windSpeed = getClass('.windSpeed');
const description = getClass('.description');

const imgDay = "url('./images/day.jpg')";
const imgNight = "url('./images/dark.jpg')";

const valueInput = (e) => {
    e.preventDefault();
    const inputValue = form['weatherText'].value;
    if(!inputValue){
        console.log("nop");
    } else {
        displayCity(inputValue);
        form.reset();
    }
}

const KtoC = (k) => {
    return Math.floor(k - 273.15);
}

const displayCity = async(city) => {
    const cityInfo = await apiFetch(city);
    if(cityInfo === "Not found"){
        description.innerHTML = cityInfo;
        return;
    }

    const { main, weather, wind, name } = cityInfo;
    const imageIcon = weather[0].icon;

    weather.map(weatherInfo => {
        temp.innerHTML = `<div>Weather<br><h2>${weatherInfo.main}</h2></div>`;
        description.innerHTML = `<h1>${weatherInfo.description}<h1>`;
    })
    icon.innerHTML = `<img src="https://openweathermap.org/img/wn/${imageIcon}@2x.png"/>`
    nameCity.innerHTML = `<h2>${name}</h2>`;
    deg.innerHTML = `<div>Degré<h1>${KtoC(main.temp)}°C</h1></div>`;
    humid.innerHTML = `<div>Humidity <br><h2>${main.humidity}%</h2></div>`;
    windSpeed.innerHTML = `<div>Wind speed <br><h2>${wind.speed}km/h</h2></div>`;

    const style = document.createElement("style");
    let styleElem = container.appendChild(style);
    const background = `
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: top top;
    `;
    if(imageIcon.includes('d')){
        styleElem.innerHTML = `
            .container::before {
                background: ${imgDay};
                ${background};
            }
        `;
        container.style.backgroundImage = imgDay;
        containerBox.style.backgroundImage = imgDay;
        document.body.classList.remove('dark');
    } else {
        styleElem.innerHTML = `
            .container::before {
                background: ${imgNight};
                ${background};
            }
        `;
        container.style.backgroundImage = imgNight;
        containerBox.style.backgroundImage = imgNight;
        document.body.classList.add('dark');
    }
}

const app = () => {
    form.addEventListener('submit', valueInput);
}
app();
